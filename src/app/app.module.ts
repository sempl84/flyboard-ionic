import {NgModule, Injectable, InjectionToken} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
// import {SocketIoModule, SocketIoConfig} from 'ngx-socket-io';
import {SocketIoModule, SocketIoConfig} from 'ng-socket-io';

import {defaultsSetting} from './setting/defaultsSetting';
import {Socket} from 'ng-socket-io';


// console.log(config);


@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot({animated: false}),
        AppRoutingModule,
        // SocketIoModule.forRoot(config) ///  remove before fly :)
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
