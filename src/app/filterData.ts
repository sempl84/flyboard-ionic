export class FilterData {
    private data = {};

    soothe(key: string, value: string, windowSize: number): any {
        if (this.data[key] === undefined) {
            this.data[key] = [];
        }
        const data = (this.data[key] as []);
        // @ts-ignore
        (this.data[key] as []).push(parseFloat(value));
        if ((this.data[key] as []).length > windowSize) {
            (this.data[key] as []).shift();
        }
        // @ts-ignore
        return (this.data[key] as []).reduce((a, b) => (a + b)) / (this.data[key] as []).length + '';
    }
}
