export const helpers = {

    toRadians(degrees): number {
        return degrees * Math.PI / 180;
    },

    toDegrees(radians): number {
        return radians * 180 / Math.PI;
    },

    formatTime(minutes: number): string {
        if (!minutes || minutes === Infinity) {
            return '00:00';
        }
        // console.log(minutes);
        let result = '';
        if (minutes >= 60) {
            // tslint:disable-next-line:no-bitwise
            result = '' + Math.floor(minutes / 60) + ':' + minutes % 60;
        } else {
            if (minutes > 0) {
                const min = (minutes < 10) ? '0' + minutes : minutes;
                result = '00:' + min;
            } else {
                result = '00:00';
            }

        }
        return result;
    }


};

