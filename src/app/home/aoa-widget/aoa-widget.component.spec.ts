import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AoaWidgetComponent} from './aoa-widget.component';

describe('AoaWidgetComponent', () => {
    let component: AoaWidgetComponent;
    let fixture: ComponentFixture<AoaWidgetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AoaWidgetComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AoaWidgetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
