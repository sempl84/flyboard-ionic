import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'aoa-widget',
    templateUrl: './aoa-widget.component.html',
    styleUrls: ['./aoa-widget.component.scss'],
})
export class AoaWidgetComponent implements OnInit {

    @Input() AOA: number;
    @Input() ANORM: number;
    public AoaColor = 'ff0000';

    constructor() {
    }

    ngOnInit() {
    }

    getPositiveAoA(): number {
        let result = 0;
        if (this.ANORM > 0) {
            result = Math.round(this.ANORM * 6.75);
        }
        if (result > 27) {
            result = 27;
        }
        if (result < 2) {
            this.AoaColor = '#1cdb1c';
        } else if (result >= 2 && result <= 3) {
            this.AoaColor = '#ffe500';
        } else {
            this.AoaColor = '#ff0000';
        }
        // console.log(this.AOA);
        return result;
    }

    getNegativeAoA(): number {
        let result = 0;
        if (this.ANORM < 0) {
            result = Math.round(this.ANORM * 13) * -1;
        }
        if (result > 26) {
            result = 26;
        }
        const aoa = Math.abs(this.ANORM);
        if (aoa < 1) {
            this.AoaColor = '#1cdb1c';
        } else if (aoa >= 1 && aoa <= 1.5) {
            this.AoaColor = '#ffe500';
        } else {
            this.AoaColor = '#ff0000';
        }
        // console.log(this.AOA);
        return result;
    }
}
