import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FuelsChartsComponent} from './fuels-charts.component';

describe('FuelsChartsComponent', () => {
    let component: FuelsChartsComponent;
    let fixture: ComponentFixture<FuelsChartsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FuelsChartsComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FuelsChartsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
