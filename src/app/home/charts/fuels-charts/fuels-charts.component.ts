import {Component, OnInit, Input, ChangeDetectionStrategy, OnChanges, ChangeDetectorRef} from '@angular/core';
import {SettingService} from '../../../setting.service';
import {Isetting} from '../../../interfaces/Isetting';
import {IAnimateSetting} from '../../../interfaces/IAnimateSetting';

@Component({
    selector: 'fuels-charts',
    templateUrl: './fuels-charts.component.html',
    styleUrls: ['./fuels-charts.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FuelsChartsComponent implements OnInit, OnChanges {

    @Input() fuelData1: any;
    @Input() fuelData2: any;
    @Input() settingKey: string;
    public setting: Isetting;
    public scale = [];
    public time = 100; /// изменение в мс
    public step = 0.05; /// шаг прироста
    public valueL = 0; /// текущее значение указателя
    public valueToL = 0; /// куда растем/падаем
    public valueR = 0; /// текущее значение указателя
    public valueToR = 0; /// куда растем/падаем
    public colorL = '';
    public colorR = '';
    constructor(
        private ch: ChangeDetectorRef,
        private service: SettingService,
    ) {
        const animateSetting = (this.service.getValue('animate_TANK') as IAnimateSetting);
        this.time = animateSetting.time;
        this.step = animateSetting.step;
    }

    ngOnInit() {
    }

    ngOnChanges(changes: any): void {
        // console.log(this.fuelData1);
        this.valueToL = this.fuelData1.value;
        this.valueToR = this.fuelData2.value;
        this.setting = (this.service.getValue('TANK') as Isetting);
        const animateSetting = (this.service.getValue('animate_TANK') as IAnimateSetting);
        this.time = animateSetting.time;
        this.step = animateSetting.step;
        this.scale = this.service.getScale(this.setting);
        this.animate();
    }

    getScale(val: number): string {
        let showValue = 0;
        const setting: Isetting = this.service.convertSetting(this.service.getValue('TANK'));
        if (val <= setting.danger.min) { // для графика
            showValue = 0;
        } else if (val < setting.danger.max) {
            showValue = (val - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min);
        } else if ((val >= setting.danger.max)) {
            showValue = 100;
        }
        return showValue.toFixed(3);
    }

    animate(): void {

        const directionL = (this.valueL <= this.valueToL) ? this.step : this.step * -1;
        const directionR = (this.valueR <= this.valueToR) ? this.step : this.step * -1;
        const setting: Isetting = this.service.convertSetting(this.service.getValue('TANK'));
        const intL = setInterval(() => {
            this.valueL = this.valueL + directionL;
            this.colorL = this.service.getColor(setting, this.valueL);
            this.ch.detectChanges();
            if (directionL > 0) {
                if (this.valueL >= this.valueToL) {
                    clearInterval(intL);
                }
            } else {
                if (this.valueL <= this.valueToL) {
                    clearInterval(intL);
                }
            }
        }, this.time);

        const intR = setInterval(() => {
            this.valueR = this.valueR + directionR;
            this.colorR = this.service.getColor(setting, this.valueR);
            this.ch.detectChanges();
            if (directionR > 0) {
                if (this.valueR > this.valueToR) {
                    clearInterval(intR);
                }
            } else {
                if (this.valueR < this.valueToR) {
                    clearInterval(intR);
                }
            }
        }, this.time);

    }

}
