import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapRpmChartComponent} from './map-rpm-chart.component';

describe('MapRpmChartComponent', () => {
    let component: MapRpmChartComponent;
    let fixture: ComponentFixture<MapRpmChartComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MapRpmChartComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MapRpmChartComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
