import {Component, OnInit, Input, ChangeDetectionStrategy, OnChanges, ChangeDetectorRef} from '@angular/core';
import {SettingService} from '../../../setting.service';
import {IGaugeSetting} from '../../../interfaces/IGaugeSetting';
import {Isetting} from '../../../interfaces/Isetting';
import {IAnimateSetting} from "../../../interfaces/IAnimateSetting";

@Component({
    selector: 'map-rpm-chart',
    templateUrl: './map-rpm-chart.component.html',
    styleUrls: ['./map-rpm-chart.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapRpmChartComponent implements OnInit, OnChanges {
    @Input() mapData: any;
    @Input() rpmData: any;
    private colors = {danger: '#ff0000', idle: '#828282', normal: '#1cdb1c', warning: '#ffe500'};
    private timeMAP = 0;
    private timeRPM = 0;
    private stepMAP = 0;
    private stepRPM = 0;
    public valueToRPM = 0;
    public valueToMAP = 0;
    public valueRPM = 0;
    public valueMAP = 0;

    constructor(
        public setting: SettingService,
        private ch: ChangeDetectorRef,
    ) {

    }

    ngOnInit() {
        this.getAnimateSetting();
    }

    getAnimateSetting() {
        const animateSettingMAP = (this.setting.getValue('animate_MAP') as IAnimateSetting);
        // console.log(animateSettingMAP);
        this.timeMAP = animateSettingMAP.time;
        this.stepMAP = animateSettingMAP.step;
        const animateSettingRPM = (this.setting.getValue('animate_RPM') as IAnimateSetting);
        this.timeRPM = animateSettingRPM.time;
        this.stepRPM = animateSettingRPM.step;
    }
    ngOnChanges(changes: any): void {
        this.getAnimateSetting();
        this.valueToMAP = this.mapData.value;
        this.valueToRPM = this.rpmData.value;
        this.animate();
    }

    animate(): void {

        const directionMAP = (this.valueMAP <= this.valueToMAP) ? this.stepMAP : this.stepMAP * -1;
        const directionRPM = (this.valueRPM <= this.valueToRPM) ? this.stepRPM : this.stepRPM * -1;
        // const setting: Isetting = this.se.convertSetting(this.service.getValue('TANK'));
        const intMAP = setInterval(() => {
            this.valueMAP = this.valueMAP + directionMAP;
            // console.log(this.valueMAP);
            this.calculateScale('mapData');
            // this.colorL = this.service.getColor(setting, this.valueL);
            this.ch.detectChanges();
            if (directionMAP > 0) {
                if (this.valueMAP >= this.valueToMAP) {
                    clearInterval(intMAP);
                }
            } else {
                if (this.valueMAP <= this.valueToMAP) {
                    clearInterval(intMAP);
                }
            }
        }, this.timeMAP);

        const intRPM = setInterval(() => {
            this.valueRPM = this.valueRPM + directionRPM;
            this.calculateScale('rpmData');
            // this.colorR = this.service.getColor(setting, this.valueR);
            this.ch.detectChanges();
            if (directionRPM > 0) {
                if (this.valueRPM >= this.valueToRPM) {
                    clearInterval(intRPM);
                }
            } else {
                if (this.valueRPM <= this.valueToRPM) {
                    clearInterval(intRPM);
                }
            }
        }, this.timeRPM);

    }

    /**
     * Расчитываем цену пикселя, цвет и высоту дуги
     * @param string key
     */
    calculateScale(key: string) {
        const data = (key === 'mapData') ? this.valueMAP : this.valueRPM;
        const settingKey = (key === 'mapData') ? 'MAP' : 'RPM';
        const setting = this.setting.getValue(settingKey);
        // if (!data || setting) {
        //     console.log([data, setting, settingKey]);
        //     return;
        // }
        const scaleHeight = {
            idle: 44,
            _55: 84,
            _65: 125,
            _75: 166,
            _100: 207,
            to: 248,
            max: 290,
        };
        let showValue = 0;
        let scale = 0;
        if (data <= setting.min) {
            showValue = 0;
        } else if (data > setting.min && data <= setting.idle) {
            showValue = Math.ceil((scaleHeight.idle / setting.idle) * data);

        } else if (data > setting.idle && data <= setting._55) {

            scale = (scaleHeight._55 - scaleHeight.idle) / (setting._55 - setting.idle);
            showValue = Math.ceil(((scale) * (data - setting.idle)) + scaleHeight.idle);

        } else if (data > setting._55 && data <= setting._65) {

            scale = (scaleHeight._65 - scaleHeight._55) / (setting._65 - setting._55);
            showValue = Math.ceil(((scale) * (data - setting._55)) + scaleHeight._55);

        } else if (data > setting._65 && data <= setting._75) {

            scale = (scaleHeight._75 - scaleHeight._65) / (setting._75 - setting._65);
            showValue = Math.ceil(((scale) * (data - setting._65)) + scaleHeight._65);

        } else if (data > setting._75 && data <= setting._100) {

            scale = (scaleHeight._100 - scaleHeight._75) / (setting._100 - setting._75);
            showValue = Math.ceil(((scale) * (data - setting._75)) + scaleHeight._75);

        } else if (data >= setting._100 && data < setting.Takeoff) {

            scale = (scaleHeight.to - scaleHeight._100) / (setting.Takeoff - setting._100);
            showValue = Math.ceil(((scale) * (data - setting._100)) + scaleHeight._100);

        } else if (data >= setting.Takeoff && data < setting.max) {

            scale = (scaleHeight.max - scaleHeight.to) / (setting.max - setting.Takeoff);
            showValue = Math.ceil(((scale) * (data - setting.Takeoff)) + scaleHeight.to);

        } else if (data >= setting.max) {
            showValue = scaleHeight.max;
        }
        let color = '';
        let labelColor = '';
        if (data <= setting.idle) {
            color = this.colors.idle;
        } else if (data > setting.idle && data <= setting._100) {
            color = this.colors.normal;
        } else if (data > setting._100 && data <= setting.Takeoff && data) {
            color = this.colors.warning;
            labelColor = this.colors.warning;
        } else if (data > setting.Takeoff) {
            color = this.colors.danger;
            labelColor = this.colors.danger;
        }
        this[key].shown = showValue;
        // console.log(showValue);
        this[key].labelColor = labelColor;
        this[key].color = color;
        if (key === 'rpmData') {
            this[key].value = Math.round(this[key].value);
        }
        this.setting.shownData.MAP1 = this.valueMAP;
        this.setting.shownData.TACH1 = this.valueRPM;
        this.setting.checkForFuelFlow();
    }

}
