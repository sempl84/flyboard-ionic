import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SmallChartsComponent} from './small-charts.component';

describe('SmallChartsComponent', () => {
    let component: SmallChartsComponent;
    let fixture: ComponentFixture<SmallChartsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SmallChartsComponent],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SmallChartsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
