import {Component, OnInit, Input, ChangeDetectionStrategy, OnChanges, ChangeDetectorRef} from '@angular/core';
import {Isetting} from '../../../interfaces/Isetting';
import {SettingService} from '../../../setting.service';
import {IAnimateSetting} from '../../../interfaces/IAnimateSetting';

@Component({
    selector: 'small-chart',
    templateUrl: './small-charts.component.html',
    styleUrls: ['./small-charts.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SmallChartsComponent implements OnInit, OnChanges {

    @Input() data: any;
    @Input() settingKey: string;
    @Input() title: string;
    @Input() subtitle: string;
    @Input() dataKey: string;
    public setting: Isetting;
    public scale = [];
    public format = '1.0-1';
    public value = 0;
    public valueTo = 0;
    public color = '';
    public time = 100; /// изменение в мс
    public step = 0.05; /// шаг прироста

    constructor(
        private settingProvider: SettingService,
        private ch: ChangeDetectorRef,
    ) {
    }

    getAnimateSetting() {
        const animateSetting = (this.settingProvider.getValue('animate_' + this.settingKey) as IAnimateSetting);
        this.time = animateSetting.time;
        this.step = animateSetting.step;
    }

    ngOnInit() {
        this.getAnimateSetting();
        if (this.settingKey === 'OIL_T' || this.settingKey === 'EGT') {
            this.format = '1.0-0';
        }
        if (this.settingKey === 'FUEL_P') {
            this.format = '1.2-2';
        }
    }

    ngOnChanges(changes: any): void {
        this.getAnimateSetting();
        this.scale = [];
        this.data.value = parseFloat(this.data.value + '');
        // console.log([this.settingKey, this.data.value]);
        this.valueTo = this.data.value;
        this.setting = (this.settingProvider.getValue(this.settingKey) as Isetting);
        this.scale = this.settingProvider.getScale(this.setting);
        this.animate();
    }

    animate(): void {
        const direction = (this.value <= this.valueTo) ? this.step : this.step * -1;
        const setting = this.setting;
        const intL = setInterval(() => {
            this.value = this.value + direction;
            this.settingProvider.shownData[this.dataKey] = this.value;
            const avgArray = ['CHT1', 'CHT2', 'CHT3', 'CHT4', 'EGT1', 'EGT2', 'EGT3', 'EGT4'];
            if (avgArray.filter((elem) => elem === this.dataKey).length) {
                // console.log([this.dataKey, this.settingProvider.shownData[this.dataKey]]);
                this.settingProvider.calculateAvg(this.value, this.settingKey);
            } else {
                this.settingProvider.setValueToArray(this.settingKey, this.dataKey, this.value);
                // window.settingArr = this.settingProvider.shownData;
            }
            this.color = this.settingProvider.getColor(setting, this.value);
            this.ch.detectChanges();
            if (direction > 0) {
                console.log(this.value);
                if (this.value >= this.valueTo) {
                    clearInterval(intL);
                }
            } else {
                if (this.value <= this.valueTo) {
                    clearInterval(intL);
                }
            }
        }, this.time);
    }

    getScale(val: number): string {
        let showValue = 0;
        const setting: Isetting = this.settingProvider.convertSetting(this.settingProvider.getValue(this.settingKey));

        if (val <= setting.danger.min) { // для графика
            showValue = 0;
        } else if (val < setting.danger.max) {
            showValue = (val - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min);
        } else if ((val >= setting.danger.max)) {
            showValue = 100;
        }
        // console.log([val, showValue, setting]);
        return showValue.toFixed(3);
    }
}
