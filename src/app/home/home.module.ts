import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {FuelsChartsComponent} from './charts/fuels-charts/fuels-charts.component';
import { HomePage } from './home.page';
import {SmallChartsComponent} from './charts/small-charts/small-charts.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MapRpmChartComponent} from './charts/map-rpm-chart/map-rpm-chart.component';
import {MainpanelComponent} from './mainpanel/mainpanel.component';
import {NavigationComponent} from './navigation/navigation.component';
import {AoaWidgetComponent} from './aoa-widget/aoa-widget.component';
import {SettingComponent} from '../setting/setting/setting.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {
        path: 'setting',
        component: SettingComponent
      }
    ])
  ],
  declarations: [
    AoaWidgetComponent,
    HomePage,
    FuelsChartsComponent,
    SmallChartsComponent,
    MapRpmChartComponent,
    MainpanelComponent,
    NavigationComponent,
    SettingComponent
  ]
})
export class HomePageModule {}
