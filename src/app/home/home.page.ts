import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ToastController} from '@ionic/angular';
import {SocketsService} from '../sockets.service';
import {SettingService} from '../setting.service';
import {Isetting} from '../interfaces/Isetting';
import {FormBuilder, FormGroup} from '@angular/forms';
import {IGaugeSetting} from '../interfaces/IGaugeSetting';
import {AppRoutingPreloaderService} from '../appRoutingPreloaderService';
import {INavigationData} from '../interfaces/INavigationData';
import {Router} from '@angular/router';
import {FilterData} from '../filterData';
import {IAnimateSetting} from '../interfaces/IAnimateSetting';

interface IDataFromServer {
    code: string;
    value: any;

}

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.css'],
})

export class HomePage implements OnInit {

    public filterInterface: FilterData;
    public settingKeys = [
        'OILTe', 'OILPe', 'VOLT', 'CURRNT', 'CHT1', 'CHT2', 'CHT3', 'CHT4', 'EGT1', 'EGT2', 'EGT3', 'EGT4',
        'FUELF', 'FUELP', 'FUELQ1', 'FUELQ2', 'GS'
    ];
    public gaugeKeys = [
        'MAP1', 'TACH1'
    ];

    public navigationKeys = [
        'SSKYWPT',
        'GS',
        'LAT',
        'LONG',
        'TRACK',
        'HEAD',
        'TIMEZ',
        'OAT',
        'DEVANGLE', // БУ
        'DISTANCE', // Расстояние до WP (м)
        'NEXTDECL', // ???
        'NEXTWPT', // название WP
        'NEXTCOURSE', // ЗПУ
        'NEXTLAT', // координаты точки следования широта
        'NEXTLONG', // координаты точки следования долгота
        'BEARING', // пеленг
        'DEVIATION', // отклонение (м)
        'WINGSPEED', // скорость ветра
        'WINGHEAD', // Направление ветра
    ];
    public mainKeys = [
        'GS',
        'TAS',
        'VS',
        'IAS',
        'BARO',
        'AIRPRESS',
        'ALT',
        'TALT',
        'ROLL',
        'PITCH',
        'AOA',
        'ANORM',
        'SLIP',
        'HEAD'
    ];

    public navigationData = {
        SSKYWPT: '',
        GS: 0,
        LAT: 0,
        LONG: 0,
        TRACK: 0,
        HEAD: 0,
        TIMEZ: 0,
        OAT: 0,
        DEVANGLE: 0, // БУ
        DISTANCE: 0, // Расстояние до WP (м)
        NEXTDECL: 0, // ???
        NEXTWPT: '', // название WP
        NEXTCOURSE: 0, // ЗПУ
        NEXTLAT: 0, // координаты точки следования широта
        NEXTLONG: 0, // координаты точки следования долгота
        BEARING: 0, // пеленг
        DEVIATION: 0, // отклонение (м)
        WINGSPEED: 0, // скорость ветра
        WINGHEAD: 0, // Направление ветра
    };

    public mainData = {
        GS: 0,
        TAS: 0,
        VS: 0,
        IAS: 0,
        BARO: 0,
        AIRPRESS: 0,
        ALT: 0,
        TALT: 0,
        ROLL: 0,
        PITCH: 0,
        AOA: 0,
        ANORM: 0,
        SLIP: 0,
        HEAD: 0,
    };

    private Code2Setting = {
        TAS: '', //  истинная скорость
        IAS: '', //  Приборная скорость
        VS: '', //  Вертикальная скорость ?
        GS: 'GS', //  скорость по gps (Путевая скорость)
        BARO: '', //  Настройка альтимерта
        AIRPRESS: '', //  Давление воздуха
        ALT: '', //  Абсолютная Высота? (от уровня моря)
        TALT: '', //  истинная высота
        LAT: '', //  широта
        LONG: '', //  долгота
        TRACK: '', //  пеленг самолета https://helpiks.org/3: '', // 94778.html
        OAT: '', //  температура забортного воздуха
        HEAD: '', //  магнитный курс самолета https://lektsii.org/5: '', // 20163.html
        ROLL: '', //  угол крена
        PITCH: '', //  тангаж
        AOA: '', //  угол атаки
        VOLT: 'DC_V', //  напряжение бортсети V
        CURRNT: 'DC_A', //  сила тока А?
        FUELF: 'TANK', //  расход топлива (гал/час)
        FUELP: 'FUEL_P', //  Давление топлива (в впускном коллекторе)
        FUELQ1: 'TANK', //  остаток топлива в баке 1
        FUELQ2: 'TANK', //  остаток топлива в баке 2
        FUELQT: 'TANK', //  всего остаток бака
        MAP1: 'MAP', //  давление в впускном коллекторе (1?)
        TACH1: 'RPM', //  обороты двигателя 1
        EGT1: 'EGT', //  температура выхлопных газов (1)
        EGT2: 'EGT', //  температура выхлопных газов (2)
        EGT3: 'EGT', //  температура выхлопных газов (3)
        EGT4: 'EGT', //  температура выхлопных газов (4)
        CHT1: 'CHT', //  температура 1 цилиндра
        CHT2: 'CHT', //  температура 2 цилиндра
        CHT3: 'CHT', //  температура 3 цилиндра
        CHT4: 'CHT', //  температура 4 цилиндра
        HOBBS1: '', //  моточасы двигателя 1
        OILTe: 'OIL_T', //  Температура масла
        OILPe: 'OIL_P', //  давление масла
        FTIME: '', //  время полета
        TIMEZ: '', //  время по Гринвичу (timestamp)
        // tslint:disable-next-line:max-line-length
        ANORM: '', //  нормальное ускорение https://interneturok.ru/lesson/physics/11: '', // klass/podgotovka: '', // k: '', // ege/uskorenie: '', // normalnaya: '', // i: '', // tangentsialnaya: '', // sostavlyayuschie: '', // uskoreniya?konspekt
        SLIP: '', //  угол скольжение
        SSKYWPT: '', // название точки следования ( маяка )
        DEVANGLE: '', // БУ
        DISTANCE: '', // Расстояние до WP (м)
        NEXTDECL: '', // магнитное отклонение (видимо)
        NEXTWPT: '', // название WP
        NEXTCOURSE: '', // ЗПУ
        NEXTLAT: '', // координаты точки следования широта
        NEXTLONG: '', // координаты точки следования долгота
        BEARING: '', // пеленг
        DEVIATION: '', // отклонение (м)
        WINGSPEED: '', // Скорость ветра
        WINGHEAD: '', // направление ветра
    };


    private colors = {danger: '#ff0000', idle: '#828282', normal: '#1cdb1c', warning: '#ffe500'};
    public selectedWidget = 'oil_dc';

    public data = {
        OILTe: {alert: false, warning: false, overload: false, value: 0},
        OILPe: {alert: false, warning: false, overload: false, value: 0},
        VOLT: {alert: false, warning: false, overload: false, value: 0},
        CURRNT: {alert: false, warning: false, overload: false, value: 0},
        CHT: {color_min: '', color_max: '', overloadMax: '', overloadMin: '', min: 0, max: 0},
        CHT1: 0,
        CHT2: 0,
        CHT3: 0,
        MAP1: 0,
        TACH1: 0,
        CHT4: 0,
        EGT: {color_min: '', color_max: '', overloadMax: '', overloadMin: '', min: 0, max: 0},
        EGT1: 0,
        EGT2: 0,
        EGT3: 0,
        EGT4: 0,
        FUELF: 0,
        FUELP: {color: '', value: 0, alert: false, shown: false},
        FUELQ1: 0,
        FUELQ2: 0,
        GS: 0
    };
    public settings = {};
    public form: FormGroup;
    public showFuelF = false;
    public endurange = '';
    public range = '';
    public fuelBurn = 0;

    constructor(
        private routingService: AppRoutingPreloaderService,
        private ref: ChangeDetectorRef,
        private socket: SocketsService,
        public service: SettingService,
        private toastCtrl: ToastController,
        private formBuilder: FormBuilder,
        private router: Router
    ) {
        this.filterInterface = new FilterData();
        ref.detach();
        this.settingKeys.forEach((key) => {
            this.data[key] = {shown: 0, value: 0, color: ''};
        });

        this.gaugeKeys.forEach((key) => {
            this.data[key] = {shown: 0, value: 0, color: ''};
        });

        this.navigationKeys.forEach((key) => {
            this.navigationData[key] = 0;
        });
        this.mainKeys.forEach((key) => {
            this.mainData[key] = 0;
        });
        this.createForm();
    }

    gotoSetting(): void {
        this.router.navigateByUrl('/home/setting');
    }

    createForm() {
        const arr = {};
        this.settingKeys.forEach((code) => {
            arr[code] = [1];
        });
        this.gaugeKeys.forEach((code) => {
            arr[code] = [1];
        });
        this.form = this.formBuilder.group(arr);
        this.form.valueChanges.subscribe((e) => {

            // tslint:disable-next-line:forin
            for (const key in this.form.value) {
                const fakeData: IDataFromServer = {
                    code: key,
                    value: this.form.value[key]
                };
                this.setNewValue(fakeData);
            }
        });
    }

    /**
     * Изменить вид виджета
     * @param type
     */
    changeWidgetView(type: string): void {
        this.selectedWidget = type;
    }

    /**
     * Установить новое значение в панели
     * @param data
     */
    setNewValue(data: IDataFromServer): void {
        if (data.code === 'GS') {
            this.service.shownData.GS = data.value;
        }
        if (data.code === 'FUELQ1' || data.code === 'FUELQ2') {
            // data.value = this.filterInterface.soothe(data.code, data.value, 10);
        }
        if (data.code === 'VOLT' || data.code === 'CURRNT') {
            // data.value = this.filterInterface.soothe(data.code, data.value, 10);
        }
        if (this.Code2Setting[data.code] === undefined) {
            console.warn('Новые ключи!!!');
            console.warn(data);
            this.Code2Setting[data.code] = '';
        }
        let val: number;
        if (data.value === null) {
            return;
        }
        val = parseFloat(data.value);
        if ( // моторные данные
            this.data[data.code]
            && data.value
            && val
            && this.data[data.code].value !== val.toFixed(2)
        ) {
            data.value = val;
            if (this.gaugeKeys.filter((item) => item === data.code).length) { // большие индикаторы
                this.setMAPOrRPM(data);
            } else if (this.settingKeys.filter((item) => item === data.code).length) {
                this.setValues(data); // маленькие индикаторы и цифры
            }
            // endurance
            // @ts-ignore
            if (this.fuelBurn) {

                const endMinute = Math.floor(
                    // @ts-ignore
                    ((parseFloat(this.data.FUELQ1.value) + parseFloat(this.data.FUELQ2.value)) / parseFloat(this.fuelBurn)) * 60
                ) - 30;
                if (endMinute >= 60) {
                    // tslint:disable-next-line:no-bitwise
                    this.endurange = '' + Math.floor(endMinute / 60) + ':' + endMinute % 60;
                } else {
                    if (endMinute > 0) {
                        const min = (endMinute < 10) ? '0' + endMinute : endMinute;
                        this.endurange = '00:' + min;
                    } else {
                        this.endurange = '00:00';
                    }

                }
                // @ts-ignore
                const range = Math.floor((endMinute / 60)) * parseFloat(this.data.GS.value);
                // @ts-ignore
                this.range = (range > 0) ? '' + range + ' km' : 'N/a';
            }

            this.ref.detectChanges();
        }

        if (this.navigationData[data.code] !== undefined) {
            if (data.code === 'DISTANCE' || data.code === 'DEVIATION') {
                data.value = parseFloat(data.value as string) / 1000; // преобразуем в км
            }
            if (data.code !== 'NEXTWPT' && data.code !== 'SSKYWPT') {
                this.animate(this.navigationData, data.code, parseFloat(data.value));
                // this.navigationData[data.code] = parseFloat(data.value as string).toFixed(2);
            } else {
                this.navigationData[data.code] = data.value;
            }
        }
        if (this.mainData[data.code] !== undefined) { // основное окно
            this.animate(this.mainData, data.code, parseFloat(data.value));
            // this.mainData[data.code] = data.value;
        }
    }

    animate(data: any, key, valueTo) {
        const animateSetting = (this.service.getValue('animate_' + key) as IAnimateSetting);
        const step = animateSetting.step;
        const time = animateSetting.time;
        const direction = (data[key] <= valueTo) ? step : step * -1;
        const intL = setInterval(() => {
            data[key] = data[key] + direction;
            this.ref.detectChanges();
            if (direction > 0) {
                if (data[key] >= valueTo) {
                    clearInterval(intL);
                }
            } else {
                if (data[key] <= valueTo) {
                    clearInterval(intL);
                }
            }
        }, time);
    }

    setValues(data: IDataFromServer): void {

        let showValue = 0;
        const setting: Isetting = this.service.convertSetting(this.service.getValue(this.Code2Setting[data.code]));

        if (data.value <= setting.danger.min) { // для графика
            showValue = 0;
        } else if (data.value < setting.danger.max) {
            showValue = Math.ceil((data.value - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min));
        } else if ((data.value >= setting.danger.max)) {
            showValue = 100;
        }
        const color = this.service.getColor(setting, data.value);

        const egtArray = ['EGT1', 'EGT2', 'EGT3', 'EGT4'];
        if (egtArray.filter((elem) => elem === data.code).length) {
            data.value = Math.round(data.value / 10) * 10;
            // console.log(data.value);
        }
        const chtArray = ['CHT1', 'CHT2', 'CHT3', 'CHT4'];
        if (chtArray.filter((elem) => elem === data.code).length) {
            data.value = Math.round(data.value);
            // console.log(data.value);
        }
        const value = {
            shown: showValue,
            value: data.value.toFixed(2),
            color,
            alert: color === this.colors.danger,
            warning: color === this.colors.warning,
            overload: data.value > setting.max
        };

        /// вставить проверку на алерты

        this.data[data.code] = value;
        /// считаем средние значения для CHT или EGT
        const avgArray = ['CHT1', 'CHT2', 'CHT3', 'CHT4', 'EGT1', 'EGT2', 'EGT3', 'EGT4'];
        if (avgArray.filter((elem) => elem === data.code).length) {
            this.calculateAvg(data);
        }
    }

    /**
     * Подсчитаем среднее значение CHT или EGT
     * @param data
     */
    calculateAvg(data: IDataFromServer) {
        const key = data.code.substring(0, 3);
        const array = [];
        const setting: Isetting = this.service.getValue(key);
        for (let i = 1; i < 3; i++) {
            if (this.data[key + i] !== undefined) {
                array.push(this.data[key + i].value);
            }
        }

        const min = Math.min.apply(null, array);
        const max = Math.max.apply(null, array);
        this.data[key] = {
            min,
            max,
            overloadMax: max > setting.max,
            overloadMin: min > setting.max,
            color_min: this.service.getColor(setting, min),
            color_max: this.service.getColor(setting, max)
        };
    }

    /**
     * Установить значения map или rpm
     * @param data
     */
    setMAPOrRPM(data: IDataFromServer): void {
        const setting: IGaugeSetting = this.service.getValue(this.Code2Setting[data.code]);
        if (!setting.enable) {
            this.data[data.code] = {
                shown: 0,
                value: 0,
                color: ''
            };

            return;
        }
        if (data.code === 'TACH1') {
            data.value = Math.round(data.value / 10) * 10;
        }
        const value = {
            shown: null,
            setting,
            value: data.value.toFixed(2),
            color: null
        };
        this.data[data.code] = value;
        this.checkForFuelFlow();
    }

    /**
     * проверяем на нужные режимы
     */
    checkForFuelFlow(): void {
        this.showFuelF = false;
        // @ts-ignore
        const map = this.data.MAP1.value;
        // @ts-ignore
        const rpm = this.data.TACH1.value;
        const mapSetting: IGaugeSetting = this.service.getValue('MAP');
        const rpmSetting: IGaugeSetting = this.service.getValue('RPM');
        const fuelBurnSetting: IGaugeSetting = this.service.getValue('Fuel_Burn');
        // @ts-ignore
        const fuelFlow = this.data.FUELQ1.value + this.data.FUELQ2.value;
        if (
            (rpm >= (rpmSetting._55 - rpmSetting._55 * 1.5 / 100) && rpm <= (rpmSetting._55 + rpmSetting._55 * 1.5 / 100)) &&
            (map >= (mapSetting._55 - mapSetting._55 * 3 / 100) && map <= (mapSetting._55 + mapSetting._55 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._55;
            this.showFuelF = true;
        }
        if (
            (rpm >= (rpmSetting._65 - rpmSetting._65 * 1.5 / 100) && rpm <= (rpmSetting._65 + rpmSetting._65 * 1.5 / 100)) &&
            (map >= (mapSetting._65 - mapSetting._65 * 3 / 100) && map <= (mapSetting._65 + mapSetting._65 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._65;
            this.showFuelF = true;
        }
        if (
            (rpm >= (rpmSetting._75 - rpmSetting._75 * 1.5 / 100) && rpm <= (rpmSetting._75 + rpmSetting._75 * 1.5 / 100)) &&
            (map >= (mapSetting._75 - mapSetting._75 * 3 / 100) && map <= (mapSetting._75 + mapSetting._75 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._75;
            this.showFuelF = true;
        }
        if (
            (rpm >= (rpmSetting._100 - rpmSetting._100 * 1.5 / 100) && rpm <= (rpmSetting._100 + rpmSetting._100 * 1.5 / 100)) &&
            (map >= (mapSetting._100 - mapSetting._100 * 3 / 100) && map <= (mapSetting._100 + mapSetting._100 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._100;
            this.showFuelF = true;
        }
        if (
            // tslint:disable-next-line:max-line-length
            (rpm >= (rpmSetting.Takeoff - rpmSetting.Takeoff * 1.5 / 100) && rpm <= (rpmSetting.Takeoff + rpmSetting.Takeoff * 1.5 / 100)) &&
            (map >= (mapSetting.Takeoff - mapSetting.Takeoff * 3 / 100) && map <= (mapSetting._100 + mapSetting.Takeoff * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting.Takeoff;
            this.showFuelF = true;
        }
    }

    async ionViewDidEnter() {
        if (this.socket.connecting === false) {
            this.socket.connect(true);
            this.socket.getData().subscribe(data => {
                this.setNewValue(data);
            });
        }
        await this.routingService.preloadRoute('setting');
    }

    ngOnInit() {
        this.socket.getData().subscribe(data => {
            this.setNewValue(data);
        });
    }

    ionViewWillLeave() {
        this.socket.disconnect();
    }

    async showToast(msg) {
        const toast = await this.toastCtrl.create({
            message: msg,
            position: 'top',
            duration: 2000
        });
        toast.present();
    }
}
