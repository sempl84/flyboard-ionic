import {Component, OnInit, Input} from '@angular/core';
import {SettingService} from '../../setting.service';
import {SocketsService} from '../../sockets.service';

@Component({
    selector: 'mainpanel',
    templateUrl: './mainpanel.component.html',
    styleUrls: ['./mainpanel.component.scss'],
})
export class MainpanelComponent implements OnInit {
    @Input() TAS: number;
    @Input() GS: number;
    @Input() VS: number;
    @Input() IAS: number;
    @Input() BARO: number;
    @Input() AIRPRESS: number;
    @Input() ALT: number;
    @Input() TALT: number;
    @Input() ROLL: number;
    @Input() PITCH: number;
    @Input() AOA: number;
    @Input() ANORM: number;
    @Input() SLIP: number;
    @Input() HEAD: number;

    public screen = 1;
    public heightArray = [];
    public speedArray = [];
    public velocityMeasure = 'km';
    public distanceMeasure = 'metres';
    public pressureMeasure = 'gpa';
    public settingScale;
    public settingValues;
    private colors = {danger: '#ff0000', idle: '#828282', normal: '#1cdb1c', warning: '#ffe500'};

    constructor(private service: SettingService, private socket: SocketsService) {
        this.settingScale = this.service.getValue('HspeedScale');
        this.settingValues = this.service.getValue('HspeedValues');
        for (let i = 5800; i >= 0; i = i - 100) {
            this.heightArray.push(i);
        }
        this.makeSpeedScale();
    }

    makeSpeedScale(): void {
        for (let i = 300; i >= 0; i = i - 10) {
            const data = {label: i, val: '', valHeight: 0, colorClass: '', div: 0, color: ''};
            let divider = 0;
            let color = '';
            if (i >= this.settingScale.danger.min && i <= this.settingScale.warning.min) {

                data.colorClass = 'red';
                color = this.colors.danger;
                divider = this.settingScale.warning.min;

            } else if (i >= this.settingScale.warning.min && i <= this.settingScale.normal.min) {

                divider = this.settingScale.normal.min;
                data.colorClass = 'yellow';
                color = this.colors.warning;

            } else if (i >= this.settingScale.normal.min && i <= this.settingScale.normal.max) {

                divider = this.settingScale.normal.max;
                data.colorClass = '';
                color = this.colors.normal;

            } else if (i >= this.settingScale.normal.max && i <= this.settingScale.warning.max) {

                divider = this.settingScale.warning.max;
                data.colorClass = 'yellow';
                color = this.colors.warning;

            } else if (i >= this.settingScale.warning.max && i <= this.settingScale.danger.max) {

                divider = this.settingScale.danger.max;
                data.colorClass = 'yellow';
                color = this.colors.danger;

            } else {
                data.colorClass = 'red';
            }
            if (divider - i > 0 && divider - i < 10) {
                data.div = (divider - i) * 2.7;
                data.color = color;
            }
            for (const key in this.settingValues) {
                if (this.settingValues[key] >= i && this.settingValues[key] < i + 10) {
                    data.val = key;
                    data.valHeight = (4.6 * (this.settingValues[key] - i)) * -1;
                }
            }
            this.speedArray.push(data);
        }
    }

    ngOnInit() {
    }

    /**
     * Изменение единицы измерения
     */
    changeMeasure(type: string): void {
        if (type === 'velocity') {
            switch (this.velocityMeasure) {
                case 'km':
                    this.velocityMeasure = 'miles';
                    break;
                case 'miles':
                    this.velocityMeasure = 'kn';
                    break;
                default:
                    this.velocityMeasure = 'km';
            }
        }
        if (type === 'distance') {
            this.distanceMeasure = (this.distanceMeasure === 'metres') ? 'foots' : 'metres';
        }
        if (type === 'pressure') {
            switch (this.pressureMeasure) {
                case 'gpa':
                    this.pressureMeasure = 'torr';
                    break;
                case 'torr':
                    this.pressureMeasure = 'inHg';
                    break;
                default:
                    this.pressureMeasure = 'gpa';
            }
        }
    }

    setBaro(type) {
        const step = 1;
        const baro = (type === 'inc') ? this.BARO + step : this.BARO - step;
        this.socket.send({code: 'BARO', value: Math.round(baro), ttl: 99999}).subscribe();
    }

    getAirPress(): number {
        switch (this.pressureMeasure) {
            case 'gpa':
                return this.BARO;
                break;
            case 'torr':
                return this.BARO * 7500637;
                break;
            default:
                return this.BARO * 295300;
        }
    }

    getPitch(): number {
        return Math.round(this.PITCH);
    }

    getIasScale(): number {
        return this.getIas() * 3;
    }

    getAltView(): number {

        return Math.round(this.getAlt() * 30 / 100);
    }

    getAlt(): number {
        switch (this.distanceMeasure) {
            case 'metres':
                return this.ALT;
                break;
            default:
                return this.ALT * 3.28;
        }
    }

    getVsScale(): number {
        let vs = this.getVs() * -43;
        if (this.getVs() > 2) {
            vs = (2 * -43) + ((this.getVs() - 2) * -10);
        }
        if (this.getVs() < -2) {
            vs = 83.85 + ((this.getVs() + 2) * -10);
        }
        if (vs < -126.5) {
            vs = -126.5;
        }
        if (vs > 126.5) {
            vs = 126.5;
        }
        return vs;
    }

    getVs(): number {
        switch (this.distanceMeasure) {
            case 'metres':
                return this.VS;
                break;
            default:
                return this.VS * 196.85;
        }
    }

    getHead(): number {
        return 2160 - Math.round((this.HEAD * 6));
    }

    getIas(): number {
        switch (this.velocityMeasure) {
            case 'km':
                return Math.round(this.IAS);
                break;
            case 'miles':
                return Math.round(this.IAS * 0.621);
                break;
            default:
                return Math.round(this.IAS * 0.54);
        }
    }

    /**
     * угол скольжения
     */
    getSlimView(): number {
        let result = this.SLIP;
        if (result < -45) {
            result = 45;
        }
        if (result > 45) {
            result = 45;
        }
        return result;
    }

    /**
     * Следующий экран
     * @param id
     */
    showScreen(id: number) {
        this.screen = id;
        console.log(this.screen);
    }

}
