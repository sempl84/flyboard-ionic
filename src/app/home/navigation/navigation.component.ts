import {Component, OnInit, Input, ChangeDetectionStrategy, OnChanges} from '@angular/core';
import {INavigationData} from '../../interfaces/INavigationData';
import {helpers} from '../../helpers';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SocketsService} from '../../sockets.service';
import {defNavigationData} from '../../setting/defNavigationData';

@Component({
    selector: 'navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.css']
    // changeDetection: ChangeDetectionStrategy.OnPush
})


export class NavigationComponent implements OnInit, OnChanges {

    @Input() data: INavigationData;
    @Input() track: any; // Направление движения
    @Input() wingSpeed: number; // Скорость ветра
    @Input() wingHead: number; // Направление движения ветра
    @Input() lat: number; // широта
    @Input() lng: number; // долгота
    @Input() waypointLat: number; // широта
    @Input() waypointLng: number; // долгота
    @Input() wing = 0; // Направление движения ветра
    @Input() gs = 0; // Скорость по GPS
    @Input() oat = 0; // Температура забортного воздуха
    @Input() course = 0; // ЗПУ
    @Input() bearing: number;
    @Input() distanceToWaypoint: number;
    public timeTo = ''; // ETA
    public utcTime = '';
    public localTime = '';
    public wptEta = ''; // время прибытия на точку
    @Input() deviation = 0; // Девиация (ЛБУ)
    @Input() deviationAngle = 0; // БУ
    public turn = 0; // указатель поворота
    public deviationScale = 1; /// масштаб отклонения
    public smartSkyOn = true;
    public wptName = '';
    public showMenu = false;
    public form: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private socket: SocketsService
    ) {

    }

    createForm() {
        this.form = this.formBuilder.group({
                // < при изменении этих полей вручную смартскай перестает использоваться, указаны данные по умолчанию
                NEXTWPT: this.wptName ? this.wptName : defNavigationData.NEXTWPT,
                NEXTLAT: this.data.NEXTLAT ? this.data.NEXTLAT : defNavigationData.NEXTLAT,
                NEXTLONG: this.data.NEXTLONG ? this.data.NEXTLONG : defNavigationData.NEXTLONG,
                NEXTCOURSE: this.data.NEXTCOURSE ? this.data.NEXTCOURSE : defNavigationData.NEXTCOURSE,
                NEXTDECL: this.data.NEXTDECL ? this.data.NEXTDECL : defNavigationData.NEXTDECL,

                DISTANCE: this.data.DISTANCE,
                BEARING: this.data.BEARING,
                DEVIATION: this.data.DEVIATION,
            WINGSPEED: this.data.WINGSPEED,
            WINGHEAD: this.data.WINGHEAD,
                Smatrsky: this.smartSkyOn,
                SWaypoint: this.data.SSKYWPT,
                SLatitude: null,
                SLongtitude: null,
                SCourse: null,
                SMdeclination: null
            }
        );
        this.form.valueChanges.subscribe((e) => {
            this.smartSkyOn = false;
            const controls = this.form.controls;
            Object.keys(controls).forEach(controlName => {
                if (!controls[controlName].pristine) {
                    this.socket.send({code: controlName, value: controls[controlName].value, ttl: 99999}).subscribe();
                    controls[controlName].markAsPristine();
                }
            });
        });
    }

    togleMenu() {
        if (!this.form) {
            this.createForm();
        }
        this.showMenu = !this.showMenu;

    }

    changeSmartSkyOn() {
        this.smartSkyOn = !this.smartSkyOn;
    }

    ngOnInit() {
        this.setTimes();
    }

    /**
     * Установить время
     */
    setTimes(): void {
        // tslint:disable-next-line:new-parens
        const date = new Date;
        const minutes = (date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes();
        const utcMinutes = (date.getUTCMinutes() < 10) ? '0' + date.getUTCMinutes() : date.getUTCMinutes();
        this.utcTime = date.getUTCHours() + ':' + utcMinutes;
        this.localTime = date.getHours() + ':' + minutes;
        setTimeout(() => {
            this.setTimes();
        }, 30000);

    }

    /**
     * Поворот
     * @param turn
     */
    getTurn(turn: number): number {
        return Math.abs(turn);
    }

    /**
     * Курс
     */
    getCourse(): number {
        return this.course - this.track;
    }

    /**
     * Изменить курс тапом по областям
     * @param value
     */
    changeCourse(value: number): void {
        if (typeof (this.course) === 'string') {
            this.course = parseFloat(this.course);
        }
        this.course = this.course + value;
    }

    /**
     * направление ветра
     */
    getWing(): number {
        return this.wing - this.track;
    }

    /**
     * Расстояние до waypoint
     */
    // getDistance(): number {
    // const R = 6371e3;  // Радиус Земли, считаем округленно
    // const f1 = helpers.toRadians(this.lat);
    // const f2 = helpers.toRadians(this.waypointLat);
    // const deltaf = helpers.toRadians(this.waypointLat - this.lat);
    // const deltaL = helpers.toRadians(this.waypointLng - this.lng);
    //
    // const a = Math.sin(deltaf / 2) * Math.sin(deltaf / 2) +
    //     Math.cos(f1) * Math.cos(f2) *
    //     Math.sin(deltaL / 2) * Math.sin(deltaL / 2);
    // const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    //
    // const d = R * c;
    // return Math.round(this.distanceToWaypoint / 10) / 100;
    // }

    /**
     * Пеленг на waypoint
     */
    // getBearing(): number {
    //     const startLat = helpers.toRadians(this.lat);
    //     const startLng = helpers.toRadians(this.lng);
    //     const destLat = helpers.toRadians(this.waypointLat);
    //     const destLng = helpers.toRadians(this.waypointLng);
    //
    //     const y = Math.sin(destLng - startLng) * Math.cos(destLat);
    //     const x = Math.cos(startLat) * Math.sin(destLat) -
    //         Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng);
    //     const brng = Math.atan2(y, x);
    //     const brngDg = helpers.toDegrees(brng);
    //     return Math.floor(((brngDg + 360) % 360) * 100) / 100;
    // }

    ngOnChanges(): void {
        if (this.data.SSKYWPT) {
            this.wptName = this.data.SSKYWPT;
        }
        if (this.data.NEXTWPT) {
            this.wptName = this.data.NEXTWPT;
        }
        // this.track = parseFloat(this.track).toFixed(2);
        // this.bearing = this.getBearing();
        // this.distanceToWaypoint = this.getDistance();
        // this.deviationAngle = this.course - this.bearing;
        // this.distanceToWaypoint = this.distanceToWaypoint / 1000;
        // this.deviation = Math.round(this.deviation / 1000);
        // this.deviation= Math.round(this.deviation / 10) / 100
        // this.deviation = Math.floor((this.distanceToWaypoint * Math.sin(this.deviationAngle)) * 100) / 100;
        this.setDeviationScale();
        const etaMinutes = (this.distanceToWaypoint && this.gs) ? Math.round(this.distanceToWaypoint / (this.gs / 60)) : 0;
        this.timeTo = this.distanceToWaypoint && this.gs ? helpers.formatTime(etaMinutes) : 'N/a';
        const date = new Date();
        date.setMinutes(date.getMinutes() + etaMinutes);
        this.wptEta = (this.distanceToWaypoint && this.gs) ? date.getHours() + ':' + date.getMinutes() : 'N/a';

    }

    /**
     * Установить масштаб отклонения
     */
    setDeviationScale(): void {
        if (this.distanceToWaypoint < 1 && Math.abs(this.deviation) < 1) {
            this.deviationScale = 0.2;
        } else if (this.distanceToWaypoint <= 10) {
            this.deviationScale = 1;
        } else if (this.distanceToWaypoint <= 20) {
            this.deviationScale = 5;
        } else if (this.distanceToWaypoint > 20) {
            this.deviationScale = 10;
        }
    }

    /**
     * Отображение ЛБУ
     */
    getDeviationView(): number {
        let result = (this.deviationScale) ? Math.round((30 / this.deviationScale) * this.deviation) * -1 : 0;
        if (result > 40) {
            result = 40;
        }
        if (result < -40) {
            result = -40;
        }
        return result;
    }

    /**
     * Отображение ЛБУ в цифрах
     */
    getDeviationValue(): number {
        return Math.abs(this.deviation);
    }
}
