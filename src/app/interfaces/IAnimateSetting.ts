export interface IAnimateSetting {
    time: number;
    step: number;
}
