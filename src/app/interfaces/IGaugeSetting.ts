export interface IGaugeSetting {
    enable: boolean;
    name: string;
    min: number;
    idle: number;
    _55: number;
    _65: number;
    _75: number;
    _100: number;
    Takeoff: number;
    max: number;
    step: number;
}
