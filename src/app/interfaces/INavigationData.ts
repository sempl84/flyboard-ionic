export interface INavigationData {
    SSKYWPT: any;
    GS: any;
    LAT: any;
    LONG: any;
    TRACK: any;
    HEAD: any;
    TIMEZ: any;
    OAT: any;
    DEVANGLE: any;
    DISTANCE: any;
    NEXTDECL: any;
    NEXTWPT: any;
    NEXTCOURSE: any;
    NEXTLAT: any;
    NEXTLONG: any;
    BEARING: any;
    DEVIATION: any;
    WINGSPEED: any;
    WINGHEAD: any;
}
