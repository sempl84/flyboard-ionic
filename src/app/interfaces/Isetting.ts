export interface Isetting {
    name: string;
    unit?: string;
    minor_units?: number;
    min: any;
    max: any;
    normal?: { min: any, max: any };
    warning?: { min: any, max: any };
    danger?: { min: any, max: any };
    step?: any;
}
