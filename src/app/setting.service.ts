import {Injectable} from '@angular/core';
import {Isetting} from './interfaces/Isetting';
import {defaultsSetting} from './setting/defaultsSetting';
import {IGaugeSetting} from './interfaces/IGaugeSetting';

@Injectable({
    providedIn: 'root'
})
export class SettingService {
    private colors = {danger: '#ff0000', idle: '#828282', normal: '#1cdb1c', warning: '#ffe500'};

    public shownData: any = {};
    public fuelBurn = 0;
    public showFuelF = false;
    public endurange = '';
    public range = '';
    constructor() {

    }

    checkForFuelFlow(): void {
        this.showFuelF = false;
        // @ts-ignore
        const map = this.shownData.MAP1;
        // @ts-ignore
        const rpm = this.shownData.TACH1;
        const mapSetting: IGaugeSetting = this.getValue('MAP');
        const rpmSetting: IGaugeSetting = this.getValue('RPM');
        const fuelBurnSetting: IGaugeSetting = this.getValue('Fuel_Burn');
        // @ts-ignore
        // const fuelFlow = this.data.FUELQ1.value + this.data.FUELQ2.value;
        if (
            (rpm >= (rpmSetting._55 - rpmSetting._55 * 1.5 / 100) && rpm <= (rpmSetting._55 + rpmSetting._55 * 1.5 / 100)) &&
            (map >= (mapSetting._55 - mapSetting._55 * 3 / 100) && map <= (mapSetting._55 + mapSetting._55 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._55;
            this.showFuelF = true;
        }
        if (
            (rpm >= (rpmSetting._65 - rpmSetting._65 * 1.5 / 100) && rpm <= (rpmSetting._65 + rpmSetting._65 * 1.5 / 100)) &&
            (map >= (mapSetting._65 - mapSetting._65 * 3 / 100) && map <= (mapSetting._65 + mapSetting._65 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._65;
            this.showFuelF = true;
        }
        if (
            (rpm >= (rpmSetting._75 - rpmSetting._75 * 1.5 / 100) && rpm <= (rpmSetting._75 + rpmSetting._75 * 1.5 / 100)) &&
            (map >= (mapSetting._75 - mapSetting._75 * 3 / 100) && map <= (mapSetting._75 + mapSetting._75 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._75;
            this.showFuelF = true;
        }
        if (
            (rpm >= (rpmSetting._100 - rpmSetting._100 * 1.5 / 100) && rpm <= (rpmSetting._100 + rpmSetting._100 * 1.5 / 100)) &&
            (map >= (mapSetting._100 - mapSetting._100 * 3 / 100) && map <= (mapSetting._100 + mapSetting._100 * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting._100;
            this.showFuelF = true;
        }
        if (
            // tslint:disable-next-line:max-line-length
            (rpm >= (rpmSetting.Takeoff - rpmSetting.Takeoff * 1.5 / 100) && rpm <= (rpmSetting.Takeoff + rpmSetting.Takeoff * 1.5 / 100)) &&
            (map >= (mapSetting.Takeoff - mapSetting.Takeoff * 3 / 100) && map <= (mapSetting._100 + mapSetting.Takeoff * 3 / 100))
        ) {
            this.fuelBurn = fuelBurnSetting.Takeoff;
            this.showFuelF = true;
        }
        if (this.fuelBurn) {

            const endMinute = Math.floor(
                // @ts-ignore
                ((parseFloat(this.shownData.FUELQ1) + parseFloat(this.data.FUELQ2)) / parseFloat(this.fuelBurn)) * 60
            ) - 30;
            if (endMinute >= 60) {
                // tslint:disable-next-line:no-bitwise
                this.endurange = '' + Math.floor(endMinute / 60) + ':' + endMinute % 60;
            } else {
                if (endMinute > 0) {
                    const min = (endMinute < 10) ? '0' + endMinute : endMinute;
                    this.endurange = '00:' + min;
                } else {
                    this.endurange = '00:00';
                }

            }
            // @ts-ignore
            const range = Math.floor((endMinute / 60)) * parseFloat(this.shownData.GS);
            // @ts-ignore
            this.range = (range > 0) ? '' + range + ' km' : 'N/a';
        }
    }


    /**
     * Подсчитаем среднее значение CHT или EGT
     * @param data
     */
    calculateAvg(value, key) {

        const array = [];
        const setting: Isetting = this.getValue(key);
        for (let i = 1; i < 3; i++) {
            // console.log(key + i);
            // console.log(this.shownData[key + i]);
            if (this.shownData[key + i] !== undefined) {
                array.push(this.shownData[key + i]);
            }
        }
        // console.log([key, value, array, ]);
        const min = Math.min.apply(null, array);
        const max = Math.max.apply(null, array);
        this.shownData[key] = {
            min,
            max,
            overloadMax: max > setting.max,
            overloadMin: min > setting.max,
            color_min: this.getColor(setting, min),
            color_max: this.getColor(setting, max)
        };
        // console.log(this.shownData[key]);
    }

    /**
     * Получить значение из хранилища
     * @param key
     */
    getValue(key: string): any {

        try {
            let data: any = JSON.parse(localStorage.getItem(key));
            if (data === null) {
                // tslint:disable-next-line:no-shadowed-variable
                console.error(key);
                data = defaultsSetting[key];
                localStorage.setItem(key, JSON.stringify(data));
            }
            return data;
        } catch (e) {
            console.log(key);
        }


    }

    setValue(data: any, key: string): void {
        localStorage.setItem(key, JSON.stringify(data));
    }

    /**
     * Цвет столбика в шкале
     * @param data
     * @param value
     */
    getColor(data: Isetting, value: any): string {

        if ((value >= data.normal.min) && (value <= data.normal.max)) {
            return this.colors.normal;
        } else {
            if (
                (value >= data.warning.min) && (value <= data.warning.max)
            ) {
                return this.colors.warning;
            } else {
                return this.colors.danger;
            }
        }
    }

    /**
     * Конвертируем настройки
     * @param setting
     */
    convertSetting(setting: Isetting): Isetting {
        if (setting.danger.min === null) {
            if (setting.warning.min === null) {
                setting.danger.min = setting.normal.min;
                setting.warning.min = setting.normal.min;
            } else {
                setting.danger.min = setting.warning.min;
            }
        } else {
            if (setting.warning.min === null) {
                setting.warning.min = setting.normal.min;
            }
        }
        if (setting.danger.max === null) {
            if (setting.warning.max === null) {
                setting.danger.max = setting.normal.max;
                setting.warning.max = setting.normal.max;
            } else {
                setting.danger.max = setting.warning.max;
            }
        } else {
            if (setting.warning.max === null) {
                setting.warning.max = setting.normal.max;
            }
        }
        return setting;
    }

    /**
     * Размерность шкалы
     * @param setting
     */
    getScale(setting: Isetting): any {
        let bottom = 0;
        const scale = [];
        setting = this.convertSetting(setting);
        scale.push({
            class: 'rb',
            height: 0,
            bottom,
        });
        bottom = Math.round(
            (setting.warning.min - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min)
        );
        scale.push({
            class: 'yb',
            height: 0,
            bottom,
        });
        bottom = Math.round(
            (setting.normal.min - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min)
        );
        scale.push({
            class: 'gr',
            height: 0,
            bottom,
        });
        bottom = Math.round(
            (setting.normal.max - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min)
        );
        scale.push({
            class: 'yb',
            height: 0,
            bottom,
        });

        bottom = Math.round(
            (setting.warning.max - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min)
        );
        scale.push({
            class: 'rb',
            height: 0,
            bottom,
        });
        scale.push({
            class: '',
            height: 0,
            bottom: 100,
        });

        for (let i = 0; i < scale.length - 1; i++) {
            scale[i].height = scale[i + 1].bottom - scale[i].bottom;
        }
        return scale;
    }

    setValueToArray(settingKey, key, value) {
        let showValue = 0;
        const setting: Isetting = this.convertSetting(this.getValue(settingKey));

        if (value <= setting.danger.min) { // для графика
            showValue = 0;
        } else if (value < setting.danger.max) {
            showValue = Math.ceil((value - setting.danger.min) * 100 / (setting.danger.max - setting.danger.min));
        } else if ((value >= setting.danger.max)) {
            showValue = 100;
        }
        const color = this.getColor(setting, value);
        const newValue = {
            shown: showValue,
            value,
            color,
            alert: color === this.colors.danger,
            warning: color === this.colors.warning,
            overload: value > setting.max
        };

        /// вставить проверку на алерты

        this.shownData[key] = newValue;
    }

}
