const setting = {
        TANK: {
            name: 'TANK',
            min: 0,
            max: 55,
            normal: {min: 15, max: 50},
            warning: {min: 10, max: 55},
            danger: {min: 0, max: 55},
            step: 1,
        },
        OIL_T: {
            name: 'OIL_T',
            min: 0,
            max: 150,
            normal: {min: 90, max: 110},
            warning: {min: 50, max: 130},
            danger: {min: 50, max: 150},
            step: 1,
        },
        OIL_P: {
            name: 'OIL_P',
            min: 0,
            max: 10,
            normal: {min: 2, max: 5},
            warning: {min: 0.8, max: 7},
            danger: {min: 0, max: 10},
            step: 0.1,
        },
        DC_V: {
            name: 'DC_V',
            min: 8,
            max: 18,
            normal: {min: 12, max: 14.5},
            warning: {min: 11, max: 16},
            danger: {min: 8, max: 18},
            step: 0.1,
        },
        DC_A: {
            name: 'DC_A',
            min: -3,
            max: 10,
            danger: {min: -3, max: 10},
            warning: {min: 0, max: 7},
            normal: {min: 2, max: 5},
            step: 0.1,
        },
        CHT: {
            name: 'CHT',
            min: 50,
            max: 150,
            normal: {min: 50, max: 120},
            warning: {min: 50, max: 135},
            danger: {min: 50, max: 150},
            step: 1,
        },
        EGT: {
            name: 'EGT',
            min: 400,
            max: 1000,
            normal: {min: 400, max: 950},
            warning: {min: 400, max: 950},
            danger: {min: 400, max: 1000},
            step: 1,
        },
        FUEL_P: {
            name: 'FUEL_P',
            min: 0,
            max: 1,
            normal: {min: 0.2, max: 0.7},
            warning: {min: 0.1, max: 0.8},
            danger: {min: 0, max: 1},
            step: 0.01,
        },
        TRIM_E: {
            name: 'TRIM_E',
            min: -20,
            max: 20,
            danger: {min: null, max: null},
            warning: {min: -20, max: 20},
            normal: {min: -10, max: 10},
            step: 1,
        },
        TRIM_A: {
            name: 'TRIM_A',
            min: -20,
            max: 20,
            danger: {min: null, max: null},
            warning: {min: -20, max: 20},
            normal: {min: -10, max: 10},
            step: 1,
        },
        TRIM_R: {
            name: 'TRIM_R',
            min: -20,
            max: 20,
            danger: {min: null, max: null},
            warning: {min: -20, max: 20},
            normal: {min: -10, max: 10},
            step: 1,
        },
        Flaps: {
            name: 'Flaps',
            min: -5,
            max: 15,
            danger: {min: null, max: null},
            warning: {min: -5, max: 15},
            normal: {min: 1, max: 7},
            step: 1,
        },
    GS: {
        name: 'GS',
        min: -5,
        max: 15,
        danger: {min: null, max: null},
        warning: {min: -5, max: 15},
        normal: {min: 1, max: 7},
        step: 1,
    },
        MAP: {
            enable: true,
            name: 'MAP',
            min: 0,
            idle: 10,
            _55: 28,
            _65: 29,
            _75: 31,
            _100: 35,
            Takeoff: 39,
            max: 39,
            step: 0.1,
        },
        RPM: {
            enable: true,
            name: 'RPM',
            min: 0,
            idle: 1400,
            _55: 4300,
            _65: 4800,
            _75: 5000,
            _100: 5500,
            Takeoff: 5700,
            max: 6000,
            step: 10,
        },

        Fuel_Burn: {
            enable: true,
            name: 'Fuel_Burn',
            min: 0,
            idle: 5,
            _55: 13,
            _65: 19,
            _75: 22,
            _100: 27,
            Takeoff: 35,
            max: 35,
            step: 1,
        },
    HspeedScale: {
        danger: {min: 0, max: 201},
        warning: {min: 63, max: 165},
        normal: {min: 72, max: 164},
        gray: {min: 0, max: 110},
    },
    HspeedValues: {
        Vne: 200,
        Va: 165,
        Vc: 144,
        Vfe: 110,
        Vx: 80,
        Vy: 75,
        Vs: 71,
        Vso: 63,
    },
    animate_TANK: {
        time: 100,
        step: 0.05,
    },
    animate_OIL_T: {
        time: 50,
        step: 0.05,
    },
    animate_OIL_P: {
        time: 50,
        step: 0.05,
    },
    animate_DC_V: {
        time: 50,
        step: 0.05,
    },
    animate_DC_A: {
        time: 50,
        step: 0.05,
    },
    animate_FUEL_P: {
        time: 50,
        step: 0.05,
    },
    animate_CHT: {
        time: 50,
        step: 5,
    },
    animate_EGT: {
        time: 50,
        step: 5,
    },
    animate_GS: {
        time: 20,
        step: 0.05,
    },
    animate_TAS: {
        time: 20,
        step: 0.05,
    },
    animate_VS: {
        time: 20,
        step: 0.05,
    },

    animate_IAS: {
        time: 20,
        step: 0.05,
    },
    animate_BARO: {
        time: 20,
        step: 0.05,
    },
    animate_AIRPRESS: {
        time: 20,
        step: 0.05,
    },
    animate_ALT: {
        time: 20,
        step: 0.05,
    },
    animate_TALT: {
        time: 20,
        step: 0.05,
    },
    animate_ROLL: {
        time: 20,
        step: 0.05,
    },
    animate_PITCH: {
        time: 20,
        step: 0.05,
    },
    animate_AOA: {
        time: 20,
        step: 0.05,
    },
    animate_ANORM: {
        time: 20,
        step: 0.05,
    },
    animate_SLIP: {
        time: 20,
        step: 0.05,
    },
    animate_HEAD: {
        time: 20,
        step: 0.05,
    },
    animate_LAT: {
        time: 20,
        step: 0.05,
    },
    animate_LONG: {
        time: 20,
        step: 0.05,
    },
    animate_TRACK: {
        time: 20,
        step: 0.05,
    },
    animate_TIMEZ: {
        time: 20,
        step: 0.05,
    },
    animate_OAT: {
        time: 20,
        step: 0.05,
    },
    animate_DEVANGLE: {
        time: 20,
        step: 0.05,
    },
    animate_DISTANCE: {
        time: 20,
        step: 0.05,
    },
    animate_NEXTDECL: {
        time: 20,
        step: 0.05,
    },
    animate_NEXTCOURSE: {
        time: 20,
        step: 0.05,
    },
    animate_NEXTLAT: {
        time: 20,
        step: 0.05,
    },
    animate_NEXTLONG: {
        time: 20,
        step: 0.05,
    },
    animate_BEARING: {
        time: 20,
        step: 0.05,
    },
    animate_DEVIATION: {
        time: 20,
        step: 0.05,
    },
    animate_WINGSPEED: {
        time: 20,
        step: 0.05,
    },
    animate_: {
        time: 20,
        step: 0.05,
    },
    animate_MAP: {
        time: 20,
        step: 0.05,
    },
    animate_RPM: {
        time: 20,
        step: 0.05,
    },
    animate_WINGHEAD: {
        time: 20,
        step: 0.05,
    },
    serverUrl: 'http://144.76.168.15:3110',
};

//     LAT: 0,
//     LONG: 0,
//     TRACK: 0,
//     HEAD: 0,
//     TIMEZ: 0,
//     OAT: 0,
//     DEVANGLE: 0, // БУ
//     DISTANCE: 0, // Расстояние до WP (м)
//     NEXTDECL: 0, // ???
//     NEXTCOURSE: 0, // ЗПУ
//     NEXTLAT: 0, // координаты точки следования широта
//     NEXTLONG: 0, // координаты точки следования долгота
//     BEARING: 0, // пеленг
//     DEVIATION: 0, // отклонение (м)
//     WINGSPEED: 0, // скорость ветра
//     WINGHEAD: 0, // Направление ветра

// // GS: 0,
//     TAS: 0,
//     VS: 0,
//     IAS: 0,
//     BARO: 0,
//     AIRPRESS: 0,
//     ALT: 0,
//     TALT: 0,
//     ROLL: 0,
//     PITCH: 0,
//     AOA: 0,
//     ANORM: 0,
//     SLIP: 0,
//     HEAD: 0,
// };
// OIL_T
// OIL_P
// DC_V
// DC_A
// FUEL_P
// CHT
// EGT

export const defaultsSetting = setting;
