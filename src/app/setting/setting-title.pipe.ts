import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'settingTitle'
})
export class SettingTitlePipe implements PipeTransform {

    transform(value: any, ...args: any[]): any {

        let result = '';
        switch (value) {
            case '_55':
                result = '-55-';
                break;
            case '_65':
                result = '-65-';
                break;
            case '_75':
                result = '-75-';
                break;
            case '_100':
                result = '-100-';
                break;
            default:
                result = value;
        }
        return result;
    }

}
