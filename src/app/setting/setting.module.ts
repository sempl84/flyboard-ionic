import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingComponent} from './setting/setting.component';
import {RouterModule} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomePage} from '../home/home.page';
import {SettingTitlePipe} from './setting-title.pipe';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            // {
            //     path: '',
            //     component: SettingComponent
            // }
        ]),
        ReactiveFormsModule
    ],
    declarations: [
        // SettingComponent,
        SettingTitlePipe]
})
export class SettingModule {
}
