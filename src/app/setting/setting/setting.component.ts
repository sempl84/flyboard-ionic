import {Component, OnInit} from '@angular/core';
import {SettingService} from '../../setting.service';
import {FormGroup, FormControl, Validators, FormArray, FormBuilder, AbstractControl} from '@angular/forms';
import {Isetting} from '../../interfaces/Isetting';
import {IGaugeSetting} from '../../interfaces/IGaugeSetting';

@Component({
    selector: 'app-setting',
    templateUrl: './setting.component.html',
    styleUrls: ['./setting.component.css'],
})
export class SettingComponent implements OnInit {

    public settingKeys = [
        'TANK', 'OIL_T', 'OIL_P', 'DC_V', 'DC_A', 'CHT', 'EGT', 'FUEL_P', 'TRIM_E', 'TRIM_A', 'TRIM_R', 'Flaps'
    ];
    public gaugeKeys = [
        'MAP', 'RPM', 'Fuel_Burn',
    ];
    public hSpeedKeys = [
        'HspeedScale', 'HspeedValues',
    ];
    public animateKeys = [
        'animate_TANK', 'animate_OIL_T',
        'animate_OIL_P',
        'animate_DC_V', 'animate_DC_A',
        'animate_FUEL_P', 'animate_CHT', 'animate_EGT',
        'animate_LAT', 'animate_LONG', 'animate_TRACK',
        'animate_HEAD', 'animate_TIMEZ', 'animate_OAT',
        'animate_DEVANGLE', 'animate_DISTANCE', 'animate_NEXTDECL',
        'animate_NEXTCOURSE', 'animate_NEXTLAT', 'animate_NEXTLONG',
        'animate_BEARING', 'animate_DEVIATION', 'animate_WINGSPEED',
        'animate_WINGHEAD', 'animate_GS', 'animate_TAS', 'animate_IAS',
        'animate_BARO', 'animate_AIRPRESS', 'animate_ALT', 'animate_TALT',
        'animate_ROLL', 'animate_PITCH', 'animate_AOA', 'animate_ANORM',
        'animate_SLIP', 'animate_HEAD', 'animate_MAP', 'animate_RPM',
    ];
    public data = [];
    public gaugeData = [];
    public hSpeedData = [];
    public animateData = [];
    public form: FormGroup;

    constructor(private service: SettingService, private formBuilder: FormBuilder) {
        const date = new Date();
        // console.log(date.getTime());
        this.settingKeys.forEach((key) => {
            this.data.push(this.service.getValue(key));
        });
        this.gaugeKeys.forEach((key) => {
            this.gaugeData.push(this.service.getValue(key));
        });
        this.hSpeedKeys.forEach((key) => {
            this.hSpeedData.push(this.service.getValue(key));
        });
        this.animateKeys.forEach((key) => {
            this.animateData.push(this.service.getValue(key));
        });
        this.createForm();
    }

    getAnimateLabel(i: number) {
        return this.animateKeys[i].replace('animate_', '');
    }

    ionViewDidEnter() {

    }

    ionViewWillEnter() {
    }

    ngOnInit() {
    }

    createForm(): void {
        const array = [];

        const arrayGauge = [];
        const arraySpeed = [];
        const arrayAnimate = [];
        this.data.forEach((item: Isetting) => {
            array.push(
                new FormGroup({
                    danger_min: new FormControl(item.danger.min, []),
                    danger_min_toggle: new FormControl(item.danger.min !== null, []),
                    danger_max: new FormControl(item.danger.max, []),
                    danger_max_toggle: new FormControl(item.danger.max !== null, []),
                    warning_min: new FormControl(item.warning.min, []),
                    warning_min_toggle: new FormControl(item.warning.min !== null, []),
                    warning_max: new FormControl(item.warning.max, []),
                    warning_max_toggle: new FormControl(item.warning.max !== null, []),
                    normal_min: new FormControl(item.normal.min, []),
                    normal_min_toggle: new FormControl(item.normal.min !== null, []),
                    normal_max: new FormControl(item.normal.max, []),
                    normal_max_toggle: new FormControl(item.normal.max !== null, []),
                }));
        });
        this.gaugeData.forEach((item: IGaugeSetting) => {
            arrayGauge.push(
                new FormGroup({
                    enable: new FormControl(item.enable, []),
                    min: new FormControl(item.min, []),
                    idle: new FormControl(item.idle, []),
                    _55: new FormControl(item._55, []),
                    _65: new FormControl(item._65, []),
                    _75: new FormControl(item._75, []),
                    _100: new FormControl(item._100, []),
                    Takeoff: new FormControl(item.Takeoff, []),
                    max: new FormControl(item.max, []),
                }));
        });
        this.hSpeedData.forEach((item: any, index) => { // Шкала скорости
            console.log(item);
            if (index === 0) {
                arraySpeed.push(
                    new FormGroup({
                        danger_min: new FormControl(item.danger.min, []),
                        danger_max: new FormControl(item.danger.max, []),
                        warning_min: new FormControl(item.warning.min, []),
                        warning_max: new FormControl(item.warning.max, []),
                        normal_min: new FormControl(item.normal.min, []),
                        normal_max: new FormControl(item.normal.max, []),
                        gray_min: new FormControl(item.gray.min, []),
                        gray_max: new FormControl(item.gray.max, []),
                    }));
            } else {
                const obj = {};
                // tslint:disable-next-line:forin
                for (const key in item) {
                    obj[key] = new FormControl(item[key], []);
                }
                arraySpeed.push(new FormGroup(obj));
            }
        });
        this.animateData.forEach((item: any, index) => { // Анимация
            // console.log(item);
            if (item === undefined) {
                console.log([this.animateData[index]]);
            } else {
                arrayAnimate.push(
                    new FormGroup({
                        time: new FormControl(item.time, []),
                        step: new FormControl(item.step, []),

                    }));
            }

        });

        this.form = this.formBuilder.group({
            serverUrl: this.service.getValue('serverUrl'),
            setting: this.formBuilder.array(array),
            settingGauge: this.formBuilder.array(arrayGauge),
            speed: this.formBuilder.array(arraySpeed),
            animate: this.formBuilder.array(arrayAnimate),
        });
        // console.log(this.form);
        this.form.valueChanges.subscribe((e) => {
            this.service.setValue(this.form.get('serverUrl').value, 'serverUrl');
            for (const index in (this.form.get('setting') as FormArray).controls) {
                const controls = ((this.form.get('setting') as FormArray).controls[index] as FormGroup).controls;
                // tslint:disable-next-line:forin
                for (const key in controls) {
                    const item = (controls[key] as FormControl);
                    if (!item.pristine) {
                        const value = item.value;
                        if (parseInt(index, 10) < this.settingKeys.length) {
                            const name = this.settingKeys[index];
                            const elem = this.data[index];
                            const params = (key as string).split('_', 2);
                            elem[params[0]][params[1]] = value;
                            const values = Object.keys(elem).map((prop, i, keys) => {
                                if (typeof elem[prop] === 'object') {
                                    return [elem[prop].min, elem[prop].max];
                                }
                            });
                            // tslint:disable-next-line:no-unused-expression
                            const valuesArray = (values as any).flat().filter(element => {
                                return element !== undefined && element !== null;
                            });
                            console.log(valuesArray);
                            elem.min = Math.min.apply(null, valuesArray);
                            elem.max = Math.max.apply(null, valuesArray);
                            this.service.setValue(elem, name);
                        }
                        item.markAsPristine();
                    }
                }
            }
            for (const index in (this.form.get('settingGauge') as FormArray).controls) {
                const controls = ((this.form.get('settingGauge') as FormArray).controls[index] as FormGroup).controls;
                // tslint:disable-next-line:forin
                for (const key in controls) {
                    const item = (controls[key] as FormControl);
                    if (!item.pristine) {
                        const value = item.value;
                        if (parseInt(index, 10) < this.gaugeKeys.length) {
                            const name = this.gaugeKeys[index];
                            const elem = this.gaugeData[index];
                            elem[key] = value;
                            console.log(elem);
                            this.service.setValue(elem, name);
                        }
                        item.markAsPristine();
                    }
                }
            }
            for (const index in (this.form.get('speed') as FormArray).controls) {
                // @ts-ignore
                // tslint:disable-next-line:triple-equals
                if (index == 0) {
                    const controls = ((this.form.get('speed') as FormArray).controls[index] as FormGroup).controls;
                    // tslint:disable-next-line:forin
                    for (const key in controls) {
                        const item = (controls[key] as FormControl);
                        if (!item.pristine) {
                            const value = item.value;
                            if (parseInt(index, 10) < this.settingKeys.length) {
                                const name = 'HspeedScale';
                                const elem = this.hSpeedData[index];
                                const params = (key as string).split('_', 2);
                                elem[params[0]][params[1]] = value;
                                const values = Object.keys(elem).map((prop, i, keys) => {
                                    if (typeof elem[prop] === 'object') {
                                        return [elem[prop].min, elem[prop].max];
                                    }
                                });
                                // tslint:disable-next-line:no-unused-expression
                                const valuesArray = (values as any).flat().filter(element => {
                                    return element !== undefined && element !== null;
                                });
                                console.log(valuesArray);
                                elem.min = Math.min.apply(null, valuesArray);
                                elem.max = Math.max.apply(null, valuesArray);
                                this.service.setValue(elem, name);
                            }
                            item.markAsPristine();
                        }
                    }
                }
                // @ts-ignore
                // tslint:disable-next-line:triple-equals
                if (index == 1) {
                    const controls = ((this.form.get('speed') as FormArray).controls[index] as FormGroup).controls;
                    const oldData = this.service.getValue('HspeedValues');
                    // tslint:disable-next-line:forin
                    for (const key in controls) {
                        const item = (controls[key] as FormControl);
                        if (!item.pristine) {
                            oldData[key] = item.value;
                            item.markAsPristine();
                        }
                    }
                    this.service.setValue(oldData, 'HspeedValues');
                }

            }
            for (const index in (this.form.get('animate') as FormArray).controls) {
                const controls = ((this.form.get('animate') as FormArray).controls[index] as FormGroup).controls;
                // tslint:disable-next-line:forin
                for (const key in controls) {
                    const item = (controls[key] as FormControl);
                    console.log(item);
                    if (!item.pristine) {
                        const value = item.value;
                        if (parseInt(index, 10) < this.animateKeys.length) {
                            const name = this.animateKeys[index];
                            const elem = this.animateData[index];
                            console.log([name, elem, key]);
                            elem[key] = item.value;
                            this.service.setValue(elem, name);
                        }
                        item.markAsPristine();
                    }
                }
            }
        });

    }

    getControls(key: string): AbstractControl[] {
        return (this.form.controls[key] as FormArray).controls;
    }

    changeState(key: string, toggleKey: string, index: number): void {

        const controls = ((this.form.get('setting') as FormArray).controls[index] as FormGroup).controls;

        if (controls[toggleKey].value === false) {
            controls[key].setValue(null);
            controls[key].markAsDirty();
        } else {
            controls[key].setValue(0);
        }
        controls[key].updateValueAndValidity();
    }
}
