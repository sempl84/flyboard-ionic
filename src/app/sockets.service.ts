import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import * as io from 'socket.io-client';
import {defaultsSetting} from './setting/defaultsSetting';

interface IDataFromServer {
    code: string;
    value: any;
    ttl: number;

}

import 'rxjs/add/operator/map';
import {SocketIoConfig} from 'ng-socket-io';

@Injectable({
    providedIn: 'root'
})
export class SocketsService {
    private socket: any;
    public connecting = false;

    constructor() {
        this.createNewConnect();
    }

    createNewConnect(): void {
        const serverUrl = this.getUrl();
        // const config: SocketIoConfig = {url: serverUrl + '', options: {origins: '*:*'}};
        this.socket = io(serverUrl);
        this.socket.on('connect', () => this.connected());
        this.socket.on('disconnect', () => this.disconnected());
        this.socket.on('error', (error: string) => {
            console.log(`ERROR: "${error}" (${serverUrl})`);
        });
    }

    connected(): void {
        this.connecting = true;
    }

    disconnected(): void {
        this.connecting = false;
    }

    getUrl(): string {
        let serverUrl = JSON.parse(localStorage.getItem('serverUrl'));
        if (serverUrl === null) {
            serverUrl = defaultsSetting.serverUrl;
            localStorage.setItem('serverUrl', JSON.stringify(serverUrl));
        }
        return serverUrl;
    }

    send(data: IDataFromServer): Observable<any> {
        return new Observable<any>(observer => {
            this.socket.emit('valueSet', data, (res) => {
                if (res.success) {
                    observer.next(res.msg);
                } else {
                    observer.error(res.msg);
                }
                observer.complete();
            });
        });

    }

    getData(): Observable<any> {
        return new Observable<any>(observer => {
            this.socket.on('value', msg => {
                observer.next(msg);
            });
        });
    }

    disconnect(): void {
        this.socket.disconnect();
    }

    connect(forceReload = false): void {
        if (forceReload) {
            this.createNewConnect();
        } else {
            this.socket.connect();
        }
    }
}
